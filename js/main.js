$(document).ready(function() {
	$('.ce-d').dotdotdot();

	w = 0;
	$('#pagination li').each(function() {
		w = w + $(this).width() + 10;
	});
	$('#pagination').css('width', w+'px');

	$('#crw-rw').magnificPopup({
		type: 'inline',
		preloader: true,
	});

	$('.vi-m, .vi-b .title').click(function() {
		var attr = $(this).parent().find('.vi-fd').attr('style');
		if (typeof attr !== 'undefined' && attr !== false && $(this).parent().find('.vi-fd').css('display') == 'block') {
			$(this).parent().find('.vi-m').css('background','url(../img/btd.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideUp('slow');
		} else if (typeof attr !== 'undefined' && attr !== false && $(this).parent().find('.vi-fd').css('display') == 'none') {
			$(this).parent().find('.vi-m').css('background','url(../img/btu.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideDown('slow');
		} else {
			$(this).parent().find('.vi-m').css('background','url(../img/btu.png) no-repeat top center');
			$(this).parent().find('.vi-fd').slideDown('slow');
		}
		return false;
	});
});